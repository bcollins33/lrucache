# LRU Cache #

Bitmap Image Cache library with the ability to set limits on cache size and cache file number before evicting bitmap from the cache. By default the cache uses a least recently used policy, but can be set to a custom policy. 

## Assumptions ##

* Cache requires the developer to know when a new version of a bitmap based on a key should be reinserted into the cache. 
* Key was used as a string, as a 32-byte buffer could be converted to a string, or a URL could be shortened or used as the key. In the test cases, a fake URL is converted to a byte key and to hex string to define a key. 

## Example ##

```java
ImageCache cache = new ImageCache(maxCacheSizeInBytes, maxCacheFileLimit);
cache.put(key, bitmap); 

//to get bitmap the cache
Bitmap bitmap = cache.get(key);
if(bitmap != null){
    //do something with the bitmap 
}
```

## Custom Eviction policy ##

A custom eviction policy is possible by also passing that through the EvictionPolicy interface in when constructing the cache instance. The below eliminates the largest file size verses least recently used. 

```java
ImageCache cache = new ImageCache(1024 * 1024 * 24, 10).withEvictionPolicy(new EvictionPolicy() {
            @Override
            public int trimCacheSize(LinkedHashMap<String, Bitmap> cacheMap) {
                Map.Entry<String, Bitmap> largestEntry = null;
                int largestSize = 0;
                for (Map.Entry<String, Bitmap> entry : cacheMap.entrySet()) {
                    int size = entry.getValue().getAllocationByteCount();
                    if (entry == null || size > largestSize) {
                        largestSize = size;
                        largestEntry = entry;
                    }
                }
                cacheMap.remove(largestEntry.getKey());
                //return size of cache item removed
                return largestEntry.getValue().getAllocationByteCount();
            }
        });
```