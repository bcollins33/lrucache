package cache.ben.collins.imagecache;

import android.graphics.Bitmap;

import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by mamba on 9/21/2016.
 */

public class CustomCacheTest extends BaseCacheTest {

    @Test
    public void maxSizeBasedTest() {
        //make cache size large enough to force the eviction to be based off of file size
        cache = new ImageCache(1024 * 1024 * 24, 10).withEvictionPolicy(new EvictionPolicy() {
            @Override
            public int trimCacheSize(LinkedHashMap<String, Bitmap> cacheMap) {
                Map.Entry<String, Bitmap> largestEntry = null;
                int largestSize = 0;
                for (Map.Entry<String, Bitmap> entry : cacheMap.entrySet()) {
                    int size = entry.getValue().getAllocationByteCount();
                    if (entry == null || size > largestSize) {
                        largestSize = size;
                        largestEntry = entry;
                    }
                }
                cacheMap.remove(largestEntry.getKey());
                return largestEntry.getValue().getAllocationByteCount();
            }
        });


        //first ten will be larger files then last ten
        for (int i = 0; i < 20; i++) {
            int pixels = (i > 9) ? 20 : 50;
            Bitmap bitmap = Util.createBitmap(pixels);
            cache.put(getKeyForFakeUrl(i), bitmap);
        }

        //should remove the top file size algorithms so 0-9 url keys
        //should not be found with a 10 file eviction limit
        assertEquals(10, cache.getSize());
        for (int i = 0; i < 20; i++) {
            if (i < 10) {
                assertEquals(null, cache.get(getKeyForFakeUrl(i)));
            } else {
                assertNotNull(cache.get(getKeyForFakeUrl(i)));
            }
        }
    }
}
