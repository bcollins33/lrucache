package cache.ben.collins.imagecache;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by ben on 9/21/2016.
 */

public class LruCacheTest extends BaseCacheTest {


    @Test
    public void testEvictionOnCacheSize() throws Exception {
        int bytesPerImage = Util.createBitmap(256).getAllocationByteCount();
        //create cache size so that the bitmaps size should need to be trimmed after 10 adds
        cache = new ImageCache(bytesPerImage * 10 + 10, 30);
        testLruEviction(cache);

    }

    @Test
    public void testEvictionOnFileNumberLimit() throws Exception {
        int bytesPerImage = Util.createBitmap(256).getAllocationByteCount();
        //create cache size so that the bitmaps size should need to be trimmed after 10 adds
        cache = new ImageCache(bytesPerImage * 11, 10);
        testLruEviction(cache);
    }

    @Test
    public void testLargeAmountOfFiles() throws Exception {
        cache = new ImageCache(1024 * 1024 * 50, 50);
        for (int i = 0; i < 1000; i++) {
            addItemToCache(String.format(FAKE_URL_FORMAT, i));
        }
        assertEquals(50, cache.getSize());
    }

    private void testLruEviction(ImageCache cache) {
        for (int i = 0; i < 11; i++) {
            addItemToCache(String.format(FAKE_URL_FORMAT, i));
        }
        //check that the first url is not present in cache.
        assertEquals(null, cache.get(getKeyForFakeUrl(0)));
        //check last item was added properly
        assertNotNull(cache.get(getKeyForFakeUrl(10)));
    }


}
