package cache.ben.collins.imagecache;

import android.graphics.Bitmap;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.runner.RunWith;

/**
 * Created by mamba on 9/21/2016.
 */
@RunWith(AndroidJUnit4.class)
public class BaseCacheTest {

    protected static final String FAKE_URL_FORMAT = "http://someimageurl.com/testimage-%d";
    protected ImageCache cache;

    @After
    public void clearCache() {
        if (cache != null) {
            cache.clearCache();
            cache = null;
        }
    }

    protected String getKeyForFakeUrl(int i) {
        return Util.getKeyForUrl(String.format(FAKE_URL_FORMAT, i));
    }

    public void addItemToCache(String url) {
        Bitmap bitmap = Util.createBitmap(256);
        cache.put(Util.getKeyForUrl(url), bitmap);
    }


}
