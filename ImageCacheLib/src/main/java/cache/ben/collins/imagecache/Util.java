package cache.ben.collins.imagecache;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * Created by ben on 9/21/2016.
 */

public class Util {


    public static long calculateMemoryCacheSize(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        int memoryClass = am.getMemoryClass();
        // Target ~15% of the available heap.
        return (1024L * 1024L * memoryClass / 7);
    }

    public static Bitmap createBitmap(int pixelWidth) {
        Bitmap.Config conf = Bitmap.Config.ARGB_4444;
        Bitmap bmp = Bitmap.createBitmap(pixelWidth, pixelWidth, conf);
        return bmp;
    }

    public static byte[] getMD5BytesFromUrl(String url) {
        try {
            byte[] bytesOfMessage = url.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(bytesOfMessage);
            return thedigest;
        } catch (NoSuchAlgorithmException ex) {

        } catch (UnsupportedEncodingException ex) {

        }
        return null;
    }

    public static String convertBytesToString(byte[] bytes) {
        StringBuilder builder = new StringBuilder(bytes.length * 2);
        for (byte aByte : bytes) {
            builder.append(String.format("%02x", aByte));
        }
        return builder.toString();
    }

    public static String getKeyForUrl(String url) {
        byte[] bytes = getMD5BytesFromUrl(url);
        return convertBytesToString(bytes);
    }
}
