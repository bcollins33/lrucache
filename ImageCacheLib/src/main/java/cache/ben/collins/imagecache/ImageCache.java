package cache.ben.collins.imagecache;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import java.util.LinkedHashMap;

/**
 * Created by ben on 9/21/2016.
 * <p>
 * Image Cache implementation based on evicting bitmaps from the cache based on a limit on the number
 * of cached files or the total cache size.
 */

public class ImageCache {

    private static final String TAG = ImageCache.class.getSimpleName();

    private long maxCacheSize;
    //default to 100. not a ton of research, but a high enough number to let the cache size make
    //the eviction decisions
    private int fileLimit = 100;
    private int currentCacheSize;
    private EvictionPolicy evictionPolicy;

    private LinkedHashMap<String, Bitmap> cacheMap;

    /**
     * Initialize the cache with defaults for cache size and file number limits
     *
     * @param context Uses the context to grab a percentage of the available heap to set the cache size
     */
    public ImageCache(Context context) {
        maxCacheSize = Util.calculateMemoryCacheSize(context);
        init();
    }

    /**
     * Initialize the cache with a file number limit and a default cache size.
     *
     * @param context   Uses the context to grab a percentage of the available heap to set the cache size
     * @param fileLimit Set the eviction rule based on the number files cached. The number of cache files will not exceed the file limit
     */
    public ImageCache(Context context, int fileLimit) {
        this.fileLimit = fileLimit;
        maxCacheSize = Util.calculateMemoryCacheSize(context);
        init();
    }

    /**
     * Initialize the cache with a custom file number limit and cache size
     *
     * @param cacheSize Sets the max cache size.
     * @param fileLimit Sets the file limit
     */
    public ImageCache(int cacheSize, int fileLimit) {
        this.maxCacheSize = cacheSize;
        this.fileLimit = fileLimit;
        init();
    }


    private void init() {
        cacheMap = new LinkedHashMap<>(0, .75f, true);
        //default to LRU eviction policy
        evictionPolicy = new LruEviction();
    }

    /**
     * Clears the cache
     */
    public void clearCache() {
        cacheMap.clear();
    }

    /**
     * \
     * Puts a bitmap into the cache. After determining the size of adding the bitmap, the cache will
     * determine if trimming is needed based on number of cached files and cache size. Eviction policy
     * defaults to Least Recently Used.
     *
     * @param key
     * @param bitmap
     */
    public void put(String key, Bitmap bitmap) {
        int size = bitmap.getAllocationByteCount();
        synchronized (cacheMap) {
            currentCacheSize += size;
            Log.d(TAG, String.format("Current cache size (%d/%d), total (%d/%d)", currentCacheSize,
                    maxCacheSize, cacheMap.size(), fileLimit));
            if (currentCacheSize > maxCacheSize) {
                Log.d(TAG,
                        String.format("Have to trim cache. Cache Size is %d > %d", currentCacheSize,
                                maxCacheSize));
                trimSize();
            }
            if (cacheMap.size() >= fileLimit) {
                Log.d(TAG,
                        String.format("Have to trim cache. File Size is %d > %d", cacheMap.size(),
                                fileLimit));
                trimSize();
            }
            Bitmap previous = cacheMap.put(key, bitmap);
            if (previous != null) {
                currentCacheSize -= previous.getAllocationByteCount();
            }
        }
    }


    private void trimSize() {
        while (true) {
            if (currentCacheSize < maxCacheSize && cacheMap.size() < fileLimit) {
                break;
            }
            if (cacheMap.size() == 0) {
                break;
            }

            int deletedSize = evictionPolicy.trimCacheSize(cacheMap);
            currentCacheSize -= deletedSize;

        }
    }

    /**
     * Gets the number of bitmaps stored in the cache
     *
     * @return number of cached bitmaps
     */
    public int getSize() {
        return cacheMap.size();
    }

    /**
     * Wrap a custom eviction policy into the ImageCache
     *
     * @param policy Eviction policy to use
     * @return the ImageCache instance
     */
    public ImageCache withEvictionPolicy(EvictionPolicy policy) {
        this.evictionPolicy = policy;
        return this;
    }


    /**
     * Check the cache for a specific bitmap
     *
     * @param key key to check the cache for
     * @return Bitmap if key is present in cache. Null otherwise
     */
    public Bitmap get(String key) {
        Bitmap bitmap;
        synchronized (cacheMap) {
            //get call to a linkedhashmap with access order will update the access order
            bitmap = cacheMap.get(key);
            if (bitmap != null) {
                return bitmap;
            }
        }
        return bitmap;
    }
}
