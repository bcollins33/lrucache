package cache.ben.collins.imagecache;

import android.graphics.Bitmap;

import java.util.LinkedHashMap;

/**
 * Created by ben on 9/21/2016.
 */
public interface EvictionPolicy {


    /**
     * Method to determine how to trim the cache
     * @param cacheMap the cache map to be trimmed
     * @return file size of the removed file
     */
    int trimCacheSize(LinkedHashMap<String, Bitmap> cacheMap);
}
