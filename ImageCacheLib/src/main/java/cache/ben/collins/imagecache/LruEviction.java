package cache.ben.collins.imagecache;

import android.graphics.Bitmap;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by ben on 9/21/2016.
 */
public class LruEviction implements EvictionPolicy {

    @Override
    public int trimCacheSize(LinkedHashMap<String, Bitmap> cacheMap) {
        Map.Entry<String, Bitmap> entry = cacheMap.entrySet().iterator().next();
        cacheMap.remove(entry.getKey());
        return entry.getValue().getAllocationByteCount();
    }
}

